// console.log(1234);
function element(element) {
  return document.getElementById(element);
}

// Bài 1: Xuất 3 số theo thứ tựu tăng dần
element("giaiBai1").onclick = function () {
  let contentTable = "";

  for (let i = 0; i <= 90; i += 10) {
    let contentTr = "";
    for (let j = 1; j <= 10; j++) {
      contentTr += `<td>${i + j}</td>`;
    }
    contentTable += `<tr>${contentTr}</tr>`;
  }
  element("answer_Bai1").innerHTML = contentTable;
};
// BÀi 2: In ra các số nguyên tố trong mảng

// hàm kiểm tra số nguyên tố
kiemTraSoNguyenTo = function (x) {
  for (var i = 2; i <= Math.sqrt(x); i++) {
    if (x % i == 0) {
      return 0;
    }
  }
  return 1;
};
arr_Bai2 = [];
element("addArr_Bai2").onclick = function () {
  let n = element("soNguyen_Bai2").value * 1;
  arr_Bai2.push(n);
  element("arr_Bai2").innerHTML = arr_Bai2;
};
element("giai_Bai2").onclick = function () {
  let content = "";
  arr_Bai2.forEach((item) => {
    if (kiemTraSoNguyenTo(item)) {
      content += item + "; ";
    }
  });
  element("answer_Bai2").innerHTML = content;
};

// BÀi 3: Tính S = (2 + 3 + 4 + ... + n) + 2n
element("giai_Bai3").onclick = function () {
  let n = element("n_Bai3").value * 1;
  let sum = 2 * n;
  for (let i = 2; i <= n; i++) {
    sum += i;
  }
  element("answer_Bai3").innerHTML = sum;
};

// BÀI 4: Tìm ước số của n
// hàm tìm ước
timUoc = function (n) {
  arr = [];
  for (let i = 1; i <= n / 2; i++) {
    if (n % i == 0) {
      arr.push(i);
    }
  }
  arr.push(n);
  return arr;
};
element("giai_Bai4").onclick = function () {
  let n = element("n_Bai4").value * 1;
  element("answer_Bai4").innerHTML = timUoc(n);
};

// BÀI 5: Tính ngày tháng năm
element("giai_Bai5").onclick = function () {
  let n = element("n_Bai5").value;
  let newNumber = [];
  [...n].forEach((ele) => newNumber.unshift(ele));
  joined_array = newNumber.join("");
  element("answer_Bai5").innerHTML = joined_array;
};

// BÀI 6: Tìm x nguyên dương lớn nhất, biết 1+2+3+...+x<=100
element("giai_Bai6").onclick = function () {
  let sum = 0;
  let x = null;
  for (let i = 1; sum <= 100; i++) {
    sum += i;
    x = i - 1;
  }
  element("answer_Bai6").innerHTML = x;
};
// BÀI 7: In bảng cửu chương
element("giai_Bai7").onclick = function () {
  let n = element("n_Bai7").value * 1;
  let contentTr = "";
  for (let i = 0; i <= 10; i++) {
    contentTr += `<br>` + `${n}` + " x " + `${i}` + " = " + `${n * i}`;
  }
  element("answer_Bai7").innerHTML = contentTr;
};

// BÀi 8: Tìm sinh viên xa trường nhất
element("giai_Bai8").onclick = function () {
  let arr = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  let player1 = [];
  let player2 = [];
  let player3 = [];
  let player4 = [];
  for (let i = 1; i <= 3; i++) {
    player1.push(arr.shift());
    player2.push(arr.shift());
    player3.push(arr.shift());
    player4.push(arr.shift());
  }
  element("answer_Bai8").innerHTML =
    `<br>` +
    "Player 1: " +
    `${player1}` +
    `<br>` +
    "Player 2: " +
    `${player2}` +
    `<br>` +
    "Player 3: " +
    `${player3}` +
    `<br>` +
    "Player 4: " +
    `${player4}`;
};

// BÀi 9: Tìm góc lệch kim giờ và kim phút
element("giai_Bai9").onclick = function () {
  let gio = element("gio_Bai9").value * 1;
  let phut = element("phut_Bai9").value * 1;
  // gọi O là tâm đồng hồ
  // có một trục thẳng đi từ tâm đồng hồ đến số 12 gọi là tia 0x
  // kim giờ là tia OA
  // kim phút là tia OB
  // vì chỉ cần xác định giờ và phút nên coi phút là đơn vị
  // kim phút quay 1 vòng cần 60 phút: 60 step ~ 360 độ
  // => 1 step ~ 1 phút ~ 6 độ
  // kim giờ quay 1 vòng cần 12 giờ = 720 phút: 720 step ~ 360 độ
  // => 1 step ~ 1 phút ~ 0.5 độ
  // góc giữa kim giờ và kim phút = góc xAO - góc yAO
  let gocPhut = phut * 6;
  let gocGio = (gio * 60 + phut) * 0.5;
  // góc lệch = | 0.5*(giờ*60+phút) - phút*6 |
  let output = Math.abs(gocGio - gocPhut);

  if (output > 180) {
    output = 360 - output;
  }
  element("answer_Bai9").innerHTML = output;
};

// BÀi 10: Tìm số gà và số chó
element("giaiBai10").onclick = function () {
  let m = element("m_bai10").value;
  let n = element("n_bai10").value;
  let soCho = (n - 2 * m) / 2;
  let soGa = (4 * m - n) / 2;
  element("ketquaBai10").innerHTML =
    "Có " + soCho + " con chó" + " và có " + soGa + " con gà";
};
